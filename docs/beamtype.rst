=========
Beam type
=========

Choose X-rays or electrons to have target, primary collimator etc. in the beam line, or electron foils etc.
