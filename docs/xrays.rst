======
X-rays
======

Specify the geometry for simulation of photon beams.

Target
------

Different beams use different targets for TrueBeam. To simulate the 4X, 6X, and 6FFF beams, use the Low Energy target. For 8X and 10X, use the Medium Energy target. For 10FFF, 15X and higher, use the High Energy target. For the imaging beamline, use the imaging target. To use your own target, choose custom.

Custom target
-------------

Specify properties of a cylindrical target. Position offset is relative to 100 cm from isocenter. The custom target can be one or two layers. For one layer, specify a thickness of 0 for the second layer. For two layers, give a positive thickness and a material for the both layers. The radii of the two cylinders are the same. The layers are touching. The electron beam hits the first layer first.

Flattening filter
-----------------

Choose the flattening filter corresponding the the beam simulated. For flattening-filter free beams (FFF) choose open port.

Flattening filter offset 
------------------------

Move the flattening filter from its nominal position, in 3 dimensions. Positive values move the flattening filter in the positive direction, so e.g. positive Z value will move it towards the target.

Flattening filter density
-------------------------

Change the density of the flattening filter. Not yet implemented.
