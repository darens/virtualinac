========
Plotting
========

Dose distributions and phase space files can be plotted online. In the Files tab, these files have a Plot link next to the name.

Dose distributions
------------------

For dose distributions, one dimensional plots can be created along the X, Y, and Z axes. The voxel indices to plot can be selected; to choose the central voxel automatically, specify -1. Averaging adjacent voxels may be done. For plots in the Z direction, the averaging is over X and Y. That is, specifying average: 3 will average over a 3x3 grid. For plots in the X and Y directions, averaging is only over Y and X, respectively. That is, plotting in X direction and averaging 3 will result in 3 voxels in the Y direction being averaged.

Phase spaces
------------

For plotting phase space files, the data are first binned. The value to bin (e.g. energy, X coordinate, etc.) is specified, along with the minimum and maximum number of the bins and number of bins. Binning may be restricted to a particular type of particle. The fluence may be plotted by checking the check box. This multiplies the weight of each particle by 1/cos(theta), where theta is the angle the particle direction makes with the Z (beam) axis. 

Two similar options are Factor and Condition. The particle weight for plotting may also be multiplied by an arbitrary parameter in the Factor box. A condition may be set, such that particles are binned only if a certain condition is met. Both Factor and Condition boxes accept strings to be passed to Python's eval() function. For example, to calculate the energy fluence of particles with 1 cm of the beam axis, put en in the Factor box, and abs(x)<1 && abs(y)<1 in the condition box. To access properties of the particle, use en, x, y, u, v, wt. Also, units are the same units as in the phase space files, cm and MeV.

For both types of plots
-----------------------

Two dimensional plots (color maps) can also be created. In this case the direction is the direction perpendicular to the plane.

Simulation data may be compared to measurement or other simulation data. Files with extension .dat in the file storage area will appear in the dropdown box.


