============
Beam shaping
============

Specify the positions of the components that move during treatment.

.. NOTE::
   There is inconsistency in the coordinate system used here. I'm in the process of converting to Varian scale, i.e. what is used for Developer Mode XML files.

Jaw Y1, Y2, X1, X2
------------------

Positions of the X and Y jaws. The X jaws are the ones further from the target, regardless of collimator rotation. For a field without overtravel (i.e. the jaw does not cross the beam axis) the positions of Y1 and X1 are negative, and the positions of Y2 and X2 are positive.

Collimator rotation
-------------------

Rotation of the collimator. The zero is to be specified.

Applicator
----------

Simulate one of the electron applicators. The scrapers are simulated but not the vertical tubes that hold the scrapers together. Also, only square applicators are simulated (not the 10x6 applicator).

Cutout
------

A rectangular cutout placed in the distal scraper. The size of the cutout can be specified. The material is Cerrotru and the thickness is 13.9 mm. The inside faces are vertical.

Developer Mode trajectory
-------------------------

Specify positions using a Developer Mode XML file. Files with extensions .xml in the file storage area will appear in the dropdown box. There are two parameters to specify: the number of beamlets per MU, and the number of incident electrons per MU.  Beam delivery occurs by dividing each MU in the XML file up into multiple static beams. These two parameters specify how many beams, and how many incident particles. Values will depend on the XML file and desired statistical precision.

To specify MLC positions or gantry rotations, use an XML file.

These axes are implemented:

  * gantry rotation <GantryRtn>
  * collimator rotation <CollRtn>
  * jaw positions <Y1>, <Y2>, <X1>, <X2>.
  * MLC 120 leaf positions <Mlc><B> ... <A> ...
  * couch lateral, longitudinal, vertical <CouchLat>, <CouchLng>, <CouchVrt> 
  * couch rotation <CouchRtn>

The <Energy> axis is ignored. For now you still must specify incident electron parameters and treatment head geometry explicitly. The <MLCModel> tag is also ignored--the MLC is simulated as a Millennium 120.

Not all axes positions need to be specified in the XML file. This mimics the behavior of TrueBeam. Positions that are not specified are taken from the web interface (or specified through the API). For example, one could use the same XML file for various jaw settings.

The dose file is written out at the end of the simulation.

Care must be taken when writing phase space files using an XML file. The phase space geometrical element is part of a parallel world. Currently gantry and collimator rotations do not move the position of the output phase space file. (The input phase space file is positioned correctly.)

.. Note::
   The coordinate system in the Developer Mode XML file is different from that from the web interfaces. E.g. signs on jaw positions are different, as is the 0 for gantry and collimator rotations.
