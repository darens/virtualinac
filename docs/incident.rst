=============
Incident beam
=============

Properties of the beam incident on the exit window, upstream of target or scattering foil. Parameters used to create the phase space files are tabulated in the VirtuaLinac Readme and in the FAQ on `radiotherapyresearchtools.com <http://radiotherapyresearchtools.com/montecarlo/phsp_faq.html>`_.

Number of incident particles
----------------------------

When starting from an electron beam incident on the target or foil, this specifies the number of primary electrons to simulate. When starting from a phase space file, this specifies the number of particles to read from the file. If the number specified is greater than the number of particles in the phase space file, the file will be re-read.

Energy
------

The mean energy of the incident electrons. Not used for phase space file input.

Energy spread
-------------

The energy spectrum of the incident electrons is taken to be a Gaussian. This value is the sigma of the distribution. Not used for phase space file input.

Spot size X and Y
-----------------

The lateral distribution of the incident electrons is taken to be a two-dimensional Gaussian function. These parameters specify the sigmas of the function along the X and Y axis. These values are for where the particles are created in the simulation, slightly upstream of the exit window. Not used for phase space file input.

Spot position X and Y
---------------------

The spot may be moved off axis. This feature has been removed from the web interface and is now accessible only through the API. Not used for phase space file input.

Beam angle X and Y
------------------

The incident beam may be at an angle to the beam axis. More precisely these values are the mean values of the angles of the incident electrons. See angular divergence, following. This feature has been removed from the web interface and is now accessible only through the API. Not used for phase space file input.

Angular divergence X and Y
--------------------------

The angle of incidence of the electrons in the incident beam may be chosen from a Gaussian distribution. This value is the sigma of the distribution. See beam angle, above, to set the mean value. Not used for phase space file input.

Phase space input
-----------------

A phase space file may be used as the source of particles. Both the header and phase space file must be in the file directory for the file name to appear in the dropdown box or be used through the API. The number of particles to be read from the phase space file can be set in two ways. One way is to specify the number of particles read in the 'Number of incident particles' box. If this number is greater than the number of particles in the phase space file, the file will be read again from the beginning. Alternatively, to use each particle in the phase space file an integer number of times, specify the integer in 'Use factor'. 

Use factor
----------

This entry appears when phase space input is chosen, and is the number of times each particle in the phase space file is used. The weights of the particles are not changed, but rather the number of original particles is multiplied by this factor. Each particle in the phase space file is used N times consecutively, where N is the use factor. Thus for XML trajectories it is not recommended to have a use factor > 1 (use splitting instead).


