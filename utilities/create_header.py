#!/usr/bin/env python
"""Read a binary phase space file, keeping track of particle statistics.
Write out a header file. Optionally, default values for header keys
not determined from the phase space file can be read from a template
file.
 
Parameter RECORD_LENGTH sets the number of bytes per particle. Currently
each particle record is expected to consist of type (1 byte), energy, x,
y, u, v, and possibly weight. Other values ignored.
"""

# Copyright (c) 2014-2016, Varian Medical Systems
# All rights reserved.
#
# Released under terms of the Varian Open Source license, available at
# http://radiotherapyresearchtools.com/license
# which states, among other points, the code is not for clinical use, 
# and there is no warranty.
#
# Author:  Daren Sawkey <daren.sawkey@varian.com>
# January 19, 2015

from __future__ import print_function

import os,sys
import struct
import hashlib
import argparse
from collections import OrderedDict

def create_header(fn, 
                  record_length=21, 
                  verbosity=1, 
                  template_file=None,
                  z_present=False,
                  wt_present=True,
                  origin_present=False):

    class Particle:
        def __init__(self,name):
            bignum = 99999999999.
            self.name  = name
            self.count = 0.
            self.emin  = bignum
            self.emax  = 0.
            self.eavg  = 0.
            self.wmin  = bignum
            self.wmax  = 0.

    def _update_energy(p,energy,weight):
        energy = abs(energy)
        weight = abs(weight)
        p.count += 1.
        p.eavg = ((p.count-1)/(p.count)) * p.eavg + energy/p.count
        ## note that both if statements need to be 'if', not 'elif'
        ## both may be true for the first particles
        if energy < p.emin:
            p.emin = energy
        if energy > p.emax:
            p.emax = energy
        if weight < p.wmin:
            p.wmin = weight
        if weight > p.wmax:
            p.wmax = weight


    bignum = 99999999999.

    if (verbosity > 0):
        print('Now creating header for', fn)

    header = OrderedDict([
        ('$TITLE:'          , ''),
        ('$FILE_TYPE:'      , ''),
        ('$CHECKSUM:'       , ''),
        ('$RECORD_CONTENTS:', ''),
        ('$RECORD_CONSTANT:', ''),
        ('$RECORD_LENGTH:'  , ''),
        ('$BYTE_ORDER:'     , ''),
        ('$ORIG_HISTORIES:' , ''),
        ('$PARTICLES:'      , 0),
        ('$PHOTONS:'        , 0),
        ('$ELECTRONS:'      , 0),
        ('$POSITRONS:'      , 0),
        ('$PROTONS:'        , 0),
        ('$NEUTRONS:'       , 0),
        ('$TRANSPORT_PARAMETERS:'         , ''),
        ('$MACHINE_TYPE:'   , ''),
        ('$MONTE_CARLO_CODE_VERSION:'     , ''),
        ('$GLOBAL_PHOTON_ENERGY_CUTOFF:'  , ''),
        ('$GLOBAL_PARTICLE_ENERGY_CUTOFF:', ''),
        ('$COORDINATE_SYSTEM_DESCRIPTION:', ''),
        ('$MD5SUM:'           , ''),
        ('$BEAM_NAME:'        , ''),
        ('$FIELD_SIZE:'       , ''),
        ('$NOMINAL_SSD:'      , ''),
        ('$MC_INPUT_FILENAME:', ''),
        ('$VARIANCE_REDUCTION_TECHNIQUES:', ''),
        ('$INITIAL_SOURCE_DESCRIPTION:'   , ''),
        ('$PUBLISHED_REFERENCE:'          , ''),
        ('$AUTHORS:'        , ''),
        ('$INSTITUTION:'    , ''),
        ('$LINK_VALIDATION:', ''),
        ('$ADDITIONAL_NOTES:',''),
        ('$STATISTICAL_INFORMATION_PARTICLES:',''),
        ('$STATISTICAL_INFORMATION_GEOMETRY:' ,'')
    ])

    if template_file:
        if os.path.isfile(template_file):
            with open(template_file, 'r') as f:
                ll = f.readlines()
            i = -1
            while i < len(ll):
                ## if line starts with a $ it is a key
                ## following lines until blank space are value
                key   = ''
                value = ''
                if ll[i][0] == '$':
                    key = ll[i].rstrip('\n')
                    i += 1
                    while ll[i].strip():
                        value += (ll[i])
                        i += 1
                # do not take these values from template
                disallowed_keys = ['$PARTICLES:', '$PHOTONS:', '$ELECTRONS:',
                    '$POSITRONS:', '$NEUTRONS:', '$PROTONS:', '$CHECKSUM:',
                    '$MD5SUM:',
                    '$STATISTICAL_INFORMATION_PARTICLES:',
                    '$STATISTICAL_INFORMATION_GEOMETRY:'
                ]
                if key not in disallowed_keys:
                    if key is not "":
                        header[key] = value.rstrip('\n')
                    #print(key,value)
                i += 1  

    else:
        print('Cannot open template file. Skipping.')

    photon   = Particle('PHOTONS')
    electron = Particle('ELECTRONS')
    positron = Particle('POSITRONS')
    proton   = Particle('PROTONS')
    neutron  = Particle('NEUTRONS')
    particles = [photon, electron, positron, proton, neutron]

    xmin =  bignum
    xmax = -bignum
    ymin =  bignum
    ymax = -bignum
    zmin =  bignum
    zmax = -bignum

    count = -1

    numToRead = 100000

    l = 'starting'

    f = open(fn,'rb')

    while (l):
	    #TODO: call readphsp.py
        count += 1
        l = f.read(numToRead*record_length)
        for i in range(int(len(l)/record_length)):
 
            if (verbosity > 0):
                if not (i%numToRead):
                    print('Reading particle:', count*numToRead + i)

            if (1): #try:
                j = 0
                ty = struct.unpack(
                    'b',l[i*record_length+j :i*record_length+j+1])[0]
                j += 1
                en = struct.unpack(
                    'f',l[i*record_length+j :i*record_length+j+4])[0]
                j += 4
                x = struct.unpack(
                    'f',l[i*record_length+j :i*record_length+j+4])[0]
                j += 4
                y = struct.unpack(
                    'f',l[i*record_length+j :i*record_length+j+4])[0]
                j += 4
                if z_present:
                    z = struct.unpack(
                        'f',l[i*record_length+j :i*record_length+j+4])[0]
                    j += 4
                else:
                    z = 0
                u = struct.unpack(
                    'f',l[i*record_length+j:i*record_length+j+4])[0]
                j += 4
                v = struct.unpack(
                    'f',l[i*record_length+j:i*record_length+j+4])[0]
                j += 4
                if wt_present:
                    wt = struct.unpack(
                        'f',l[i*record_length+j:i*record_length+j+4])[0]
                    j += 4
                else:
                    wt = 1.
                if origin_present:
                    proc = struct.unpack(
                        'i',l[i*record_length+j:i*record_length+j+4])[0]
                    j += 4
                    vol = struct.unpack(
                        'i',l[i*record_length+j:i*record_length+j+4])[0]
                    j += 4
                else:
                    proc = 0
                    vol  = 0
                i += 1
                if (verbosity > 1):
                    print(
                        '{0:5g} {1:g} {2:12.7} {3:8.4f} {4:8.4f} {5:8.4f} '
                        '{6:8.4f} {7:8.4f} {8:8.4f} {9:6g} {10:6g}'
                        .format(count*numToRead+i,ty,en,x,y,z,u,v,wt,proc,vol))
            else: #except:
                print('error reading particle', i)
                break
      
            #photon
            if abs(ty) == 1:
                _update_energy(photon, en, wt)
            #electron
            elif abs(ty) == 2:
                _update_energy(electron, en, wt)
            #positron
            elif abs(ty) == 3:
                _update_energy(positron, en, wt)
            #proton
            elif abs(ty) == 4:
                _update_energy(proton, en, wt)
            #neutron
            elif abs(ty) == 5:
                _update_energy(neutron, en, wt)

            else:
                if verbosity > 0:
                    #print('unknown particle: type:',ty,"index:",i)
                    print('unknown particle: type: {0:3g}, index: {1:g}'
					        .format(ty,i))

            if ty < 0:
                if verbosity > 0:
                    print('type < 0!  type: {0:3g} index {1:g}'.format(ty,i))

            if x < xmin:
                xmin = x
            if x > xmax:
                xmax = x
            if y < ymin:
                ymin = y
            if y > ymax:
                ymax = y
            if z_present:
                if z < zmin:
                    zmin = z
                if z > zmax:
                    zmax = z
  
    f.close()

    header['$PHOTONS:'  ] = int(photon.count)
    header['$ELECTRONS:'] = int(electron.count)
    header['$POSITRONS:'] = int(positron.count)
    header['$PROTONS:']   = int(proton.count)
    header['$NEUTRONS:']  = int(neutron.count)
    header['$PARTICLES:'] = (
        header['$PHOTONS:'] + header['$ELECTRONS:'] + header['$POSITRONS:'] +
        header['$PROTONS:'] + header['$NEUTRONS:'])

    header['$CHECKSUM:'] = os.stat(fn).st_size
    ## sanity check
    if header['$CHECKSUM:'] != header['$PARTICLES:'] * record_length:
        print('Checksum error!')
        exit()

    # get the md5sum
    # first reread the file: redundant, better to use what was read already
    if verbosity > 1:
        print('getting the md5sum... ', end='')
    m = hashlib.md5()
    #f = file(fn,'r')
    f = open(fn,'rb')
    while True:
        t = f.read(8096)
        if len(t) == 0:
            break
        m.update(t)
    md5sum = m.hexdigest()
    f.close()
    if verbosity>1:
        print('done.')
    header['$MD5SUM:'] = md5sum

    outstring = ('//     Weight      Wmin      Wmax        <E>'
                 '          Emin          Emax   Particle\n')

    for p in particles:
        if p.count == 0:
            p.wmin = 0
            p.wmax = 0
            p.eavg = 0
            p.emin = 0
            p.emax = 0
        outstring += ('{0:13d} {1:9.5f} {2:9.5f} {3:10.6f} {4:13.6f} {5:13.6f}'
                  '   {6:15s}\n'
            .format(int(p.count), p.wmin, p.wmax, p.eavg, p.emin, p.emax, 
                    p.name))
  
    header['$STATISTICAL_INFORMATION_PARTICLES:'] = outstring.rstrip('\n')

    outstring = 'direction  min       max\n'
    outstring += '{0}     {1:8.4f} {2:8.4f}\n'.format('x', xmin, xmax)
    outstring += '{0}     {1:8.4f} {2:8.4f}\n'.format('y', ymin, ymax)
    if z_present:
        outstring += '{0}     {1:8.4f} {2:8.4f}'  .format('z', zmin, zmax)

    header['$STATISTICAL_INFORMATION_GEOMETRY:'] = outstring

    header_string = ''
    for k,v in header.items():
        header_string += k + '\n' + str(v) + '\n\n'
    header_string += '\n'

    header_fn = '.'.join(fn.split('.')[:-1]) + '.header'
    if not os.path.exists(header_fn):
        f = open(header_fn,'w')
        f.write(header_string)
        f.close()
    else:
        print(header_string)


 
def main():
    parser = argparse.ArgumentParser(description=__doc__,
                      formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('filename',
                help='Name of phase space file to read. Specify '
                '"all" to use all phase space file in the current '
                'directory.')
    parser.add_argument('--record_length',
                default=21,
                help='Number of bytes for each particle.')
    parser.add_argument('--template_file',
                help='Specify a file to use as a template header '
                'for the values not determined from phase space '
                'file.')
    parser.add_argument('--verbosity',
                help='How verbose to be (0, 1 or 2)',
                default=1)
    args = parser.parse_args()
    filename = args.filename
    record_length = int(args.record_length)
 
    if filename == 'all':
        l2 = os.listdir('.')
        l1 = [fn for fn in l2 if fn[-5:] == '.phsp' or fn[-9:] == '.IAEAphsp']
        l1.sort()
    else: 
        l1 = [filename]

    z_present = False
    wt_present = False
    origin_present = False

    ## TODO: get these from template header file
    if record_length == 25:
        wt_present = True
    
    if record_length == 37:
        z_present = True
        wt_present = True
        origin_present = True

    for fn in l1:
        create_header(
            fn,
            record_length = record_length,
            verbosity = int(args.verbosity),
            template_file = args.template_file,
            z_present = z_present,
            wt_present = wt_present,
            origin_present = origin_present
        )


if __name__ == '__main__':
    status = main()
    sys.exit(status)
