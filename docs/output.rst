======
Output
======

Specify how to record the output. Phase space files and dose distributions can be recorded.

Write phase space file
----------------------

This phase space file is designed to record the treatment beam, and has constant distance from the isoplane. The lateral extent depends on the distance from the isoplane. The component from which the particle originated (either created or scattered) and the physics process are recorded. See below for information.

Phase space position
--------------------

The distance from isocenter to record the phase space. 0 is at isocenter. 733 mm is immediately upstream of the jaws, and was the value used to create the phase space files on myVarian.com. 420 mm is downstream of the MLC and mylar window. 50 mm is useful for comparison to the electron representative beam data air scans. -500 mm can be useful for simulations of the imager.

BEAMnrc format
--------------

.. NOTE::
   Nomenclature for formats is poor. Here by BEAMnrc format I mean the \*nrc interpretation of the IAEA concept. Suggestions welcome.

The format of the phase space files is meant to be compatible with what I can understand from the website of a certain international agency. In principle the National Research Council codes read in this format. However, there can be differences. If this box is checked:

 * no neutrons are recorded in the phase space files

 * a different coordinate system is used. The origin is 100 cm from the isocenter, Z is positive towards isocenter. X and Y are still crossplane and inplane respectively, but the sign of one is different.

 * no latch information is recorded.

You may need to make modifications to the header file to get the phase space files imported into \*nrc. For the electron phase space files on MyVarian.com, change the line::

  0   // W is stored ?

to::

  1   // W is stored ?

even though W is not stored. Also explicitly set the weight of the particles to 1::

  $RECORD_CONSTANT:
  26.7    // Constant Z
  1.0     // Constant Weight


Record leakage
--------------

Create a phase space file on a spherical surface of radius 70 cm, centered approximately on the target (100 cm from isocenter). It is likely one will want to simulate the head shielding as well; see under Physics. 

Simulate phantom, and how to build a phantom
--------------------------------------------

Record dose to a voxelized phantom. There are two options: either a water phantom may be used, with number of voxels  and phantom size specified from the GUI, or the phantom may be specified in a file. In both cases the phantom position needs to be specified. Note that the value is the position of the center of the phantom, relative to isocenter. 

To specify a phantom file, select the checkbox. The phantom file must be in the file storage area, and have extension .phantom . The phantom file is in ASCII format and contains both a header and the the phantom description. 

Header lines start with a # and are the first lines in the file. There may be any number of lines in the header. Two lines are required: one to specify the number of voxels in the file, and on to specify the size of the voxels. 

An example of the header is::

  # VirtuaLinac phantom definition file
  #
  # The next 2 lines are required by VirtuaLinac:
  # Number of voxels: 5 5 5
  # Voxel size: 20 20 20 mm
  #
  # format:
  # Z changes fastest, then Y, then X.
  # Each line contains the index of the material, and the
  #    density of material in g/cm3. Exception is G4_AIR,
  #    for which the density is fixed.

.. Note::
   There is an inconsistency between phantom files and dose distribution files, in that one requires the size of each voxel and one requires the size of the phantom. This inconsistency will be removed.

The next lines in the phantom file specify the material and density of each voxel. Each line consists of an integer for material index, a space, then a float for the density. There is one voxel per line. The ordering of the voxels is the same as for the dose output: Voxels are specified such that Z changes fastest, then Y, then X. The Z index starts from the most positive value, then decreases. X and Y start from the most negative value, then increases.



The materials and indices are:

===== =======================
Index Material
===== =======================
0     G4_AIR 
1     G4_WATER
2     G4_LUNG_ICRP
3     G4_B-100_BONE
4     G4_BONE_COMPACT_ICRU
5     G4_BONE_CORTICAL_ICRP
6     G4_BRAIN_ICRP
7     G4_EYE_LENS_ICRP
8     G4_MUSCLE_SKELETAL_ICRP
9     G4_MUSCLE_STRIATED_ICRU
10    G4_SKIN_ICRP
11    G4_TISSUE_SOFT_ICRP
12    G4_Pb
13    G4_BLOOD_ICRP
14    G4_EYE_LENS_ICRP
15    G4_TESTIS_ICRP
16    G4_TISSUE_SOFT_ICRU-4
17    G4_ADIPOSE_TISSUE_ICRP
18    G4_A-150_TISSUE
19    G4_MS20_TISSUE
20    G4_MUSCLE_WITH_SUCROSE
21    G4_MUSCLE_WITHOUT_SUCROSE
22    G4_PLEXIGLASS
23    G4_Ti
===== =======================

.. NOTE::
   The specification of phantom position, and the order of the voxels, is different from previous VirtuaLinac versions.

By default, dose is not recorded if the voxel is made of air (i.e. it is recorded as 0). To record the dose to air voxels, use the REST API.

The format of the output dose distribution is the same as the the input phantom file, as described above (the header is different).

Phantom position
----------------

The phantom position is the center of the phantom. When using a Developer Mode XML input, the parameter names are Couch, e.g. CouchLng. In VirtuaLinac these specify the coordinates of the center of the phantom.

Comment
-------

This field is reproduced in the macro file, and is intended to help the user keep track of their simulations.

Output file name
----------------

The base name for output files. Phase space files will have _leakage or _field appended, depending on if they are the spherical leakage phase space file, or the flat field phase space file respectively.

Phase space latch information
-----------------------------

The volume element in which the particle was created, or last scattered, is recorded by an integer. The physics process is also recorded. These values are tabulated below.

In the web interface, these are read into the list named extra_longs. The volume is index 1 and process is element 0.

Volume of creation or last interaction:

======  ===========
Value   Volume name
======  ===========
0       None recorded
1       Exit window
2       Target
3       Shielding
4       Upper jaw
5       Lower jaw
7--12   Target
13      Primary collimator
14      Shielding
15--26  Flattening filter assembly
27--30  Base plate assembly
31--38  Bend magnet and shielding
39      Gantry weldment
41      World (including phantom)
42      Cover
43      Mylar window
44      Electron beamline (upstream of target)
45      Electron primary foil
46      Electron secondary foil
47      Electron primary foil holder
48      Shielding
50      Monitor chamber
60      Field phase space
61      Leakage phase space
63      phantom
70      Custom target layer 1
71      Custom target layer 2 
80      MLC leaves
90      Electronics etc. around jaws
91      Bend magnet shielding
92      Shielding
======  ===========

Physics process:

===== ============
Value Process name
===== ============
0     None recorded
1     eBrem
2     compt
3     annihil
4     conv
5     phot
6     eIoni
7     photonNuclear
8     electronNuclear
9     neutronInelastic
10    nCapture
11    positronNuclear
12    CoulombScat
13    Rayl
===== ============

