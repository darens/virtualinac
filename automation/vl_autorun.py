#!/usr/bin/env python
"""
   VirtuaLinac.auto_run
   ~~~~~~~~~~~~~~~~~~~~~~~

   Run a VirtuaLinac simulation from a local computer. Start an AWS spot
   instance, upload data to it, specify the simulation parameters, monitor
   the instance until the job is done, download a plot and the simulation 
   results, then terminate the instance.

   :copyright: (c) 2017 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# auto_tests.py
#
# Copyright (c) 2017 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import sys
import time
import operator
import shutil
import requests
import boto3

def get_instance(instance_id,
                 image_id=None,
                 security_group=None,
                 instance_type='c3.8xlarge',
                 spot_price='1.7'):
    """Return the ec2.Instance item from a spot request, or given an
       existing instance id."""

    if instance_id == 'spot':
        if not image_id:
            if os.getenv('VL_IMAGE_ID'):
                image_id = os.getenv('VL_IMAGE_ID')
        if not image_id:
            raise Exception('ImageID needs to be specified on command'
                                     ' line or in environment variable '
                                     'VL_IMAGE_ID.')
            ## eg. VL_IMAGE_ID=ami-b99ea4af

        if not security_group:
            if os.getenv('VL_SECURITY_GROUP'):
                security_group = os.getenv('VL_SECURITY_GROUP')
        if not security_group:
            raise Exception('Security group needs to be '
                'specified on command line or in environment variable '
                'VL_SECURITY_GROUP')
            ## e.g. VL_SECURITY_GROUP=virtualinac-test1-security
        
        SpotType = 'one-time'
        LaunchSpecification = {
          'ImageId': image_id,
          #'KeyName': key_name,  ## we don't use an access key
          'SecurityGroups': [security_group],
          'InstanceType': instance_type
        }
        instance_id = request_spot_instances(
            SpotPrice = spot_price,
            Type = SpotType,
            LaunchSpecification = LaunchSpecification)

    ec2 = boto3.resource('ec2')
    instance = ec2.Instance(instance_id)

    print('Using AWS instance with ID {0:s}\n'.format(instance_id))

    return instance


def create_params(filename):
    """Create a dictionary of parameters for the simulation."""

    params = {}
    params['filename'] = filename
    
    params['platform'] = 'TrueBeam'
    params['code_version'] = 0
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['range_cut']  = 10.0
    params['fast_simulation'] = 'True'
    params['kill_primColl'] = 1000
    params['kill_shieldColl'] = 2000
    params['kill_jaw'] = 200
    params['kill_mlc'] = 200
    params['brem_splitting'] = 100
    params['splitting_factor'] = 10

    params['beam_type']    = 0
    params['target']       = 0
    params['flattening_filter'] = '6X'
    params['energy']       = 6.5
    params['jaw_position_y1'] = -20
    params['jaw_position_y2'] =  20
    params['jaw_position_x1'] = -20
    params['jaw_position_x2'] =  20

    params['trajectory_bool'] = 'True'
    params['trajectory_file'] = 'mlc.xml'
    params['trajectory_beamlets_mu'] = 1
    params['trajectory_particles_mu'] = 100000
    params['phsp_record']  = None

    params['phantom_bool'] = 'True'
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -20
    params['phantom_voxels_x']   = 125
    params['phantom_voxels_y']   = 125
    params['phantom_voxels_z']   = 40
    params['phantom_size_x']     = 500
    params['phantom_size_y']     = 500
    params['phantom_size_z']     = 400

    print_params(params)
   
    return params

def submit_job(url, params):
    """Submit a simulation to given url, with given parameters."""
    print('\nSubmitting job.')
    submit = requests.post(url, data=params)

def get_plot(url, filename):
    """Create and download a plot of the dose distribution."""
    plt_url = url + '/fig/' + filename + '.dose'
    plt_params = {
        'direction' : 'z',
        'voxel_x'  : -1,
        'voxel_y'  : -1,
        'voxel_z'  : 3,
        'average'  : 5,
        'colormap' : 'True'
    }

    download(url, filename+'.dose', directory='fig',
             params=plt_params, localname=filename+'.2d.z.png') 

def request_spot_instances(**kwargs):
    """Request an AWS spot instance """
    client = boto3.client('ec2')

    for k, v in kwargs.items():
        if k == 'Type':
            Type = v
        elif k == 'SpotPrice':
            SpotPrice = v
        elif k == 'LaunchSpecification':
            LaunchSpecification = v

    response = client.request_spot_instances(
        SpotPrice = SpotPrice,
        Type      = Type,
        LaunchSpecification = LaunchSpecification
    )

    request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    print('Spot request id: {0:s}'.format(request_id))

    print('Waiting for spot instance to be available.')
    waiter = client.get_waiter('spot_instance_request_fulfilled')
    waiter.wait(
        SpotInstanceRequestIds = [request_id])

    print('Spot instance started.')
    response = client.describe_spot_instance_requests(
        SpotInstanceRequestIds = [request_id])

    instance_id = response['SpotInstanceRequests'][0]['InstanceId']
    print('Instance id:', instance_id)

    print('Waiting for instance to start running.')
    waiter = client.get_waiter('instance_running')
    waiter.wait(
        InstanceIds = [instance_id])
    print('Instance running. Wait for another 30 seconds.')
    time.sleep(30)

    return instance_id


def terminate_instance(instance):
    """Terminate an instance """
    client = boto3.client('ec2')
    instance_id = instance.instance_id
    
    print('Will now terminate the instance')
    instance.terminate()
    waiter = client.get_waiter('instance_terminated')
    waiter.wait(InstanceIds = [instance_id])
    print('Instance terminated.')

    ## make sure it is terminated
    response = client.describe_instances(InstanceIds = [instance_id])
    state = response['Reservations'][0]['Instances'][0]['State']['Name']

    print('Double check: Now the instance state is: {0}'.format(state))
    if state != 'terminated':
        print('*************************************')
        print('Not terminated!!!!!!!!!!!!!!!!!!!!!!!')
        print('*************************************')
        return 1
    else:
        return 0

def download(url,
             filename,
             directory='vl_files',
             localname=None,
             params=None,
             timeout=600):
    """Download a file from an instance using GET """
    
    try:
        resp = requests.get(url + '/' + directory + '/' + filename, stream=True,
                params=params, timeout=timeout)
    except requests.Timeout:
        pass

    else:
        if not localname:
            localname = filename
        if os.path.exists(localname):
            localname = (localname + '.' +
                        str(time.time()).split('.')[0])
        with open(localname, 'wb') as f:
            for chunk in resp.iter_content(chunk_size=1024):
                f.write(chunk)
        print('Downloaded {0:s}'.format(localname))

def print_params(params):
    """Print parameters of simulation."""
    print('*************************')
    print('Input parameters:\n')
    sorted_params = sorted(params.items(), key=operator.itemgetter(0))
    for k,v in sorted_params:
        print(k,v)
    print('*************************')

def monitor_instance(url, sleeptime=30, firsttime=False):
    """Check the number of running jobs; if there are running jobs, wait."""
    running_jobs = -1
    while running_jobs != 0:
        if not firsttime:
            time.sleep(sleeptime)
        firsttime = False
            
        try:
            resp = requests.get(url + '/usage')
        except: # ConnectionError:
            print('Connection error!')
        else:
            for line in resp.text.split('\n'):
                if 'Number' in line:
                    running_jobs = int(line.split()[-2])
            sys.stdout.write(time.asctime() + ' ' + url
                                + ': jobs = '
                                + str(running_jobs) + '\n')
            if running_jobs < 0:
                sys.stdout.write('Error getting number of jobs!\n')
            sys.stdout.flush()
    print('\nSimulation complete.\n')
    return running_jobs

def upload_file(url, filename):
    """Upload a file to the VirtuaLinac file storage area."""
    up_url = url + '/upload'
    files = {'file': open(filename, 'rb')}
    r = requests.post(up_url, files=files)



###############################################################################
# main starts here    
###############################################################################


def main():
    import argparse

    parser = argparse.ArgumentParser(description=__doc__,
                         formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--instance_id',
                        help='instance_id to use. Can be i-********,'
                        ' or spot. Default=spot.',
                        default='spot')
    parser.add_argument('--image_id',
                        help='If requesting an instance, specify the image id.'
                        ' Alternatively set the environment variable '
                        'VL_IMAGE_ID.')
    parser.add_argument('--filename', 
                        help='Name of file to store results.',
                        default=None)
    parser.add_argument('--security_group', 
                        help='Name of AWS security group to use. Alternatively '
                        'set the environment variable VL_SECURITY_GROUP.',
                        default=None)
    parser.add_argument('--keep_alive', 
                        help='Keep the instance alive when simulation is done.',
                        action='store_true')
    args = parser.parse_args()

    filename = args.filename

    print('\nRun a VirtuaLinac simulation from the command line.\n')

    #  - start an instance (spot). Return the ec2.Instance
    #  wait for the instance to start
    instance = get_instance(args.instance_id, args.security_group)

    #  define the instance URL 
    url = 'http://' + instance.public_dns_name

    # upload file to the instance. Inputs: name of file, url. Return: success
    upload_file(url, 'mlc.xml')

    # create the parameters for the simulation.  Return a parameter dictionary
    params = create_params(filename)

    # submit the job. Inputs: params, url
    submit_job(url, params)

    # monitor the instance until job is finished (or instance terminated).
    monitor_instance(url)

    # create a plot
    get_plot(url, filename)

    # download the simulation results
    download(url, filename+'.mac')
    download(url, filename+'.dictionary')
    download(url, filename+'.output')
    download(url, filename+'.dose')

    # terminate the instance
    if not args.keep_alive:
        terminate_instance(instance)
    else:
        print('\nInstance is still running.\n')

    print('Goodbye.\n')


if __name__ == '__main__':
    main()



